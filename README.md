# 50 Shades of Kludgey
## or 'Fetch URL metadata and publish it back'

### Requirements

    "github.com/rjp/GoOse"
    "github.com/kr/text"
    "github.com/garyburd/redigo/redis"

### How it works

Send a JSON blob with `URL` and `MessageID` to the `urls-from-ua.json`
channel.

(`MessageID` is irrelevant to the URL metadata fetching but it's used
as a contextual tag for linking requests and replies.)

e.g.
``` .json
{"URL":"http://www.dailymail.co.uk/news/article-1173207/Cinema-ban-silver-screen-pensioners-bullying-queue-jumping-stealing-biscuits.html","MessageID":2276806}
```

Metadata will be returned on the `url-metadata` channel.  (`\u003e` is just the unicode for `>`)

``` .json
{"MessageID":2276809,"URL":"https://itunes.apple.com/gb/app/bbc-iplayer/id416580485?mt=8","Title":"BBC iPlayer on the App Store","Description":"\u003e Read reviews, compare customer ratings, see screenshots and learn\n\u003e more about BBC iPlayer. Download BBC iPlayer and enjoy it on your\n\u003e iPhone, iPad and iPod touch.","Image":"http://is2.mzstatic.com/image/thumb/Purple69/v4/5f/e5/1b/5fe51bfd-0800-2084-364a-e48c9b392e34/source/1024x1024sr.jpg","Canonical":"https://itunes.apple.com/gb/app/bbc-iplayer/id416580485?mt=8","Tags":"[]","Keywords":"BBC iPlayer, Media Applications Technologies Limited, Entertainment, ios apps, app, appstore, app store, iphone, ipad, ipod touch, itouch, itunes"}
```