package main

import (
// "github.com/advancedlogic/GoOse"
    "github.com/rjp/GoOse"
    "strings"
	"fmt"
    "github.com/kr/text"
    "github.com/garyburd/redigo/redis"
    "encoding/json"
    "flag"
)

var running_in_production bool

type URLMessage struct {
    MessageID int
    URL string
}

type URLtoPost struct {
    MessageID int
    URL string
    Title string
    Description string
    Image string
    Canonical string
    Tags string
    Keywords string
    ContentType string
}

var url_message URLMessage
var url_response URLtoPost

func main() {
    flag.BoolVar(&running_in_production, "production", false, "Running in production?")
    flag.Parse()
    println("Running in production", running_in_production)

    // Create ourselves a redis client talking to localhost
    r_reader, err := redis.Dial("tcp", ":6379")
    if err != nil {
        panic(err)
    }

    r_writer, err_w := redis.Dial("tcp", ":6379")
    if err_w != nil {
        panic(err_w)
    }

    pubsub := redis.PubSubConn{r_reader}
    pubsub.Subscribe("urls-from-ua")
    pubsub.Subscribe("urls-from-ua.json")
    pubsub.Subscribe("url-metadata")

    g := goose.New( "config.json" )

    for {
        url := ""
        switch v := pubsub.Receive().(type) {
        case redis.Message:
            println("-> ", v.Channel)
            if v.Channel == "urls-from-ua" {
                url = string(v.Data)
            }
            if v.Channel == "urls-from-ua.json" {
                err := json.Unmarshal(v.Data, &url_message)
                if err != nil {
                    panic(err)
                }
                println("id->", url_message.MessageID)
                url = url_message.URL
                url_response.MessageID = url_message.MessageID
            }
        case redis.Subscription:
            // ignore
        case error:
            panic(v)
        }

        if len(url) == 0 {
            continue
        }

        article := g.ExtractFromUrl(url)

        if ! strings.Contains(article.ContentType, "text/html") {
            fmt.Println("Probably not HTML")
        }

        if article.TopNode == nil {
            fmt.Println("NILNODE ==> ", url)
        }

        url_response.Title = article.Title
        url_response.Description = ""

        desc := article.MetaDescription
        if len(desc) > 0 {
            if len(desc) > 200 {
                desc = desc[:197] + "..."
            }
            wrapped := text.Indent(text.Wrap(desc, 65), "> ")
            url_response.Description = wrapped
        } else {
            content := article.CleanedText
            if len(content) > 0 {
                if len(content) > 200 {
                    content = content[:197] + "..."
                }
                wrapped := text.Indent(text.Wrap(content, 65), "> ")
                url_response.Description = wrapped
            }
        }

        //    println("content", article.CleanedText)
        url_response.URL = article.FinalUrl
        url_response.Image = article.TopImage
        url_response.Canonical = article.CanonicalLink
        url_response.Keywords = article.MetaKeywords

        url_response.Tags = article.Tags.String()
        url_response.ContentType = article.ContentType

        fish, _ := json.Marshal(url_response)

        if running_in_production {
            r_writer.Do("publish", "url-metadata", string(fish))
        } else {
            r_writer.Do("publish", "testing-metadata", string(fish))
        }
    }
}
